class Pet:
    def __init__(self, name, species):
        self.name = name 
        self.species = species

    @property
    def rename_pet(self):
        return "You're not allowed to rename my pets!"

    @rename_pet.setter
    def rename_pet(self, value):
        if value:
            spell = input('enter the correct spell:')
            if spell == 'obliviate':
                return "what was my pets name again"
            else:
                raise ValueError("Wards closed. Intruder")


pet1 = Pet("moriko", "cat")
print(pet1.rename_pet)
pet1.rename_pet = True


