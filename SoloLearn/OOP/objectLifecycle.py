class Pet:
    def __init__(self, species, color):
        self.species = species
        self.color = color
    def __del__(self):
        print("object has been deleted")

Moriko = Pet("cat", "tortoiseshell")
Thor = Pet("dog", "black")

del Moriko
del Thor
