class Snake:
    def __init__(self, breed, length):
        self.breed = breed
        self.length = length

    def temper(self):
        print("Hissssss")

padfoot = Snake("mexican kingsnake", 1.5)
print(padfoot.length)
padfoot.temper()

#inheritance

class Tree:
    def __init__(self, barkTexture, leafShape, color):
        self.barkTexture = barkTexture
        self.leafShape = leafShape
        self.color = color

    def note(self):
        print("I like trees")

class Oak(Tree):
    def note(self):
        print("Acorns!")
        super().note()

class Sakura(Tree):
    def note(self):
        print("Lovely in spring around the Tidal Basin")
        super().note()

oak = Oak("rough", "lobed", "brown")
print(oak.leafShape)
oak.note()

#magic methods & operator overloading

class Point:
    def __init__(self, x, y):
        self.x = x
        self.y = y
    def __sub__(self, other):
        return Point(self.x - other.x, self.y -  other.y)
    def __mod__(self , other):
        return Point(self.x % other.x, self.y % other.y)
    def __eq__(self, other):
        return "I have no idea if they're equal"

first = Point(5, 7)
second = Point(3, 9)
result = first - second
remain = first % second
print(result.x)
print(result.y)
print(remain.x)
print(remain.y)
print(first == second)

