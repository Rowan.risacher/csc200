from random import randint
class Pet:
    def __init__(self, species, color):
        self.species = species
        self.color = color
        self._secretID = randint(0,10)
        self.__reallySecretID = randint(0, 100)

Moriko = Pet("cat", "tortoiseshell")
print(Moriko._secretID)
print(Moriko._Pet__reallySecretID)

