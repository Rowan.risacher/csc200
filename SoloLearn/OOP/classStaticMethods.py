from random import randint
class Pet:
    def __init__(self, species, color):
        self.species = species
        self.color = color
        self._secretID = randint(0,10)
        self.__reallySecretID = randint(0, 100)

    @classmethod
    def cat(cls, color):
            return cls("cat", color)

    @staticmethod
    def is_dog(p):
        return p.species == "dog"

Moriko = Pet("cat", "tortoiseshell")
Pinky = Pet.cat("tabby")
Dylan = Pet("dog", "golden")

print(Moriko._secretID)
print(Moriko._Pet__reallySecretID)
print(Pinky.species)
print(Pinky.color)
print(Pet.is_dog(Pinky))
print(Pet.is_dog(Dylan))
