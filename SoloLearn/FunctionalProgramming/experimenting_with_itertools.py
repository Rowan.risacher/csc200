from itertools import repeat, accumulate, takewhile, chain,  product, permutations
print(list(repeat(7, 2)))



nums = list(accumulate(range(5)))
print(nums)

letter = ['a', 'b', 'c']
comb = list(chain(nums, letter))
print(comb)

print(list(takewhile(lambda x: x < 4, comb)))

stuff = (1, 2, 3, 'a', 'b', 'c')
print(list(product(stuff, range(3))))

print(list(permutations(stuff))) #creates a lot


